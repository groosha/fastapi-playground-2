from fastapi import FastAPI

from app.handlers import setup_handlers
from app.models.request import User
from app.models.response import UserResponseOK, UserResponseError

app = FastAPI()
root_checker = setup_handlers()


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.post("/check")
async def check_user(user: User):
    try:
        root_checker.check(user)
    except ValueError as ex:
        return UserResponseError(description=str(ex))
    else:
        return UserResponseOK()
