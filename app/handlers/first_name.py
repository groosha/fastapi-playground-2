from app.checker import Checker
from app.models.request import User

data_checker = Checker()


@data_checker.register()
def name_not_sasha(user: User):
    if user.first_name == "Sasha":
        return True
    raise ValueError("User.first_name not Sasha")


@data_checker.register()
def name_longer_than_two(user: User):
    if len(user.first_name) > 2:
        return True
    raise ValueError("Too short first name")
