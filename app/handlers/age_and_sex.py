from app.checker import Checker
from app.models.request import User, Sex

data_checker = Checker()


@data_checker.register()
def male_under_65(user: User):
    if user.sex != Sex.male:
        return True
    if user.age < 65:
        return True
    raise ValueError("Only male people under 65 are allowed")


@data_checker.register()
def female_under_60(user: User):
    if user.sex != Sex.female:
        return True
    if user.age < 60:
        return True
    raise ValueError("Only female people under 65 are allowed")
