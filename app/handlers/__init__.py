from app.checker import Checker


def setup_handlers():
    from . import first_name, age_and_sex

    handlers_checker = Checker()

    handlers_checker.functions.extend(first_name.data_checker.functions)
    handlers_checker.functions.extend(age_and_sex.data_checker.functions)

    print(f"{handlers_checker.functions=}")

    return handlers_checker
