from pydantic import BaseModel


class UserResponseOK(BaseModel):
    status: str = "ACCEPT"


class UserResponseError(BaseModel):
    status: str = "REJECT"
    description: str
