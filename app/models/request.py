from enum import Enum

from pydantic import BaseModel, conint


class Sex(str, Enum):
    male = "male"
    female = "female"
    other = "other"


class User(BaseModel):
    first_name: str
    last_name: str
    age: conint(gt=0, lt=100)
    sex: Sex
