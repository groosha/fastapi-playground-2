from app.models.request import User


class Checker:
    def __init__(self):
        self.functions = []

    def register(self):
        def wrapper(func):
            self.functions.append(func)

        return wrapper

    def check(self, user: User):
        for func in self.functions:
            func(user)
